package com.orange.gitlabci.samples.stacktracedigest;

import lombok.AllArgsConstructor;
import lombok.Builder;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * A utility class that computes a stable MD5 digest out of a Java stack trace
 */
@AllArgsConstructor
@Builder
public class JavaStackDigest {

    @Builder.Default
    private boolean excludeNoSource = true;
    @Builder.Default
    private List<Pattern> includes = Collections.emptyList();
    @Builder.Default
    private List<Pattern> excludes = toPatterns(Arrays.asList(
            "\\$\\$FastClassByCGLIB\\$\\$",
            "\\$\\$EnhancerBySpringCGLIB\\$\\$",
            "^sun\\.reflect\\..*\\.invoke",
            "^com\\.sun\\.",
            "^sun\\.net\\.",
            "^java\\.lang\\.reflect\\.Method\\.invoke",
            "^net\\.sf\\.cglib\\.proxy\\.MethodProxy\\.invoke",
            "^java\\.util\\.concurrent\\.ThreadPoolExecutor\\.runWorker",
            "^java\\.lang\\.Thread\\.run$"));

    private static final String DIGEST_ALGO = "MD5";

    /**
     * Regexp to capture the Error classname from the first stack trace line
     * group 1: error classname
     */
    private static final Pattern ERROR_PATTERN = Pattern.compile("((?:[\\w\\$]+\\.){2,}[\\w\\$]+):");

    /**
     * Regexp to extract stack trace elements information
     * group 1: classname+method
     * group 2: filename (optional)
     * group 3: line number (optional)
     */
    private static final Pattern STACK_ELEMENT_PATTERN = Pattern.compile("^\\s+at\\s+((?:[\\w\\$]+\\.){2,}[\\w\\$]+)\\((?:([^:]+)(?::(\\d+))?)?\\)");

    private static List<Pattern> toPatterns(List<Object> objects) {
        return objects.stream().map((o) -> Pattern.compile((String) o)).collect(Collectors.toList());
    }

    /**
     * Computes a stable digest out of the given Java Stack Trace
     *
     * @param stackTrace Java Stack Trace
     * @return MD5 digest
     */
    public String compute(String stackTrace) throws NoSuchAlgorithmException {
        if (stackTrace == null || stackTrace.isEmpty()) {
            return null;
        }
        return compute(new ArrayDeque<>(Arrays.asList(stackTrace.split("\\n"))));
    }

    private String compute(Deque<String> stackTraceLines) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance(DIGEST_ALGO);

        // 1: extract error class from first line
        String curStackTraceLine = stackTraceLines.pop();
        Matcher errorClassMatcher = ERROR_PATTERN.matcher(curStackTraceLine);
        if (!errorClassMatcher.find()) {
            throw new RuntimeException("Unrecognized Java stack trace");
        }

        // digest: error classname
        digest.update(errorClassMatcher.group(1).getBytes());

        // 2: read all stack trace elements until stack trace is empty or we hit the next error
        int steCount = 0;
        while (!stackTraceLines.isEmpty()) {
            curStackTraceLine = stackTraceLines.getFirst();
            if (curStackTraceLine.startsWith(" ") || curStackTraceLine.startsWith("\t")) {
                // current line starts with a whitespace: is it a stack trace element ?
                Matcher stackElementMatcher = STACK_ELEMENT_PATTERN.matcher(curStackTraceLine);
                if (stackElementMatcher.find()) {
                    // current line is a stack trace element
                    steCount += 1;
                    if (!isExcluded(stackElementMatcher)) {
                        // digest: STE classname and method
                        digest.update(stackElementMatcher.group(1).getBytes());
                        // digest: line number (if present)
                        if (stackElementMatcher.group(3) != null) {
                            digest.update(stackElementMatcher.group(3).getBytes());
                        }
                    }
                }
            } else if (steCount > 0) {
                // current line doesn't start with a whitespace and we've already read stack trace elements: it looks like the next error in the stack
                break;
            }

            // move to next line
            stackTraceLines.pop();
        }

        // 3: if stack trace not empty, compute digest for next error
        if (!stackTraceLines.isEmpty()) {
            digest.update(compute(stackTraceLines).getBytes());
        }

        return toHex(digest.digest());
    }

    private boolean isExcluded(Matcher stackElementMatcher) {
        // 1: exclude elements without source info ?
        if (excludeNoSource && stackElementMatcher.group(3) == null) {
            return true;
        }

        // 2: Regex based inclusions
        String classnameAndMethod = stackElementMatcher.group(1);
        if (!includes.isEmpty()) {
            Optional<Pattern> match = includes.stream().filter((p) -> p.matcher(classnameAndMethod).find()).findFirst();
            if (!match.isPresent()) {
                return true;
            }
        }

        // 3: Regex based exclusions
        Optional<Pattern> match = excludes.stream().filter((p) -> p.matcher(classnameAndMethod).find()).findFirst();
        if (match.isPresent()) {
            return true;
        }

        // default: not excluded
        return false;
    }

    private static String toHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }
}
