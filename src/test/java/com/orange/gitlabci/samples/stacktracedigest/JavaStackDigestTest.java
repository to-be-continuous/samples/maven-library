package com.orange.gitlabci.samples.stacktracedigest;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.regex.Pattern;

public class JavaStackDigestTest {
    private static String stackTrace(String i) throws IOException {
        StringWriter writer = new StringWriter();
        InputStreamReader reader = new InputStreamReader(JavaStackDigestTest.class.getResourceAsStream("stack_trace_" + i + ".txt"));
        char[] buffer = new char[256];
        int len;
        while ((len = reader.read(buffer)) > 0) {
            writer.write(buffer, 0, len);
        }
        reader.close();
        return writer.toString();
    }

    @Test
    public void digest_should_be_computed_if_stack_trace_present() throws IOException, NoSuchAlgorithmException {
        String digest = JavaStackDigest.builder().build().compute(stackTrace("1"));
        Assert.assertEquals("e801d2223555804c354eba382af40487", digest);
    }

    @Test
    public void digest_should_not_be_computed_if_stack_trace_not_present() throws IOException, NoSuchAlgorithmException {
        String digest = JavaStackDigest.builder().build().compute(null);
        Assert.assertNull(digest);
    }

    @Test
    public void digest_with_exclusion_config_1() throws IOException, NoSuchAlgorithmException {
        String digest = JavaStackDigest.builder()
                .excludeNoSource(false)
                .build()
                .compute(stackTrace("1"));
        Assert.assertEquals("498f8151629971495ed5dd8c3713d45e", digest);
    }

    @Test
    public void digest_with_exclusion_config_2() throws IOException, NoSuchAlgorithmException {
        String digest = JavaStackDigest.builder()
                .excludeNoSource(false)
                .excludes(Arrays.asList(Pattern.compile("\\$\\$FastClassByCGLIB\\$\\$"), Pattern.compile("\\$\\$EnhancerBySpringCGLIB\\$\\$")))
                .build()
                .compute(stackTrace("1"));
        Assert.assertEquals("ff887f4f5d9e588df37afbb4ceee6980", digest);
    }

    @Test
    public void digest_with_inclusion_and_exclusion_config() throws IOException, NoSuchAlgorithmException {
        String digest = JavaStackDigest.builder()
                .excludeNoSource(true)
                .includes(Arrays.asList(Pattern.compile("^com\\.xyz\\.")))
                .excludes(Arrays.asList(Pattern.compile("\\$\\$FastClassByCGLIB\\$\\$"), Pattern.compile("\\$\\$EnhancerBySpringCGLIB\\$\\$")))
                .build()
                .compute(stackTrace("1"));
        Assert.assertEquals("fa54771601828e9a2964ed0651e8c09c", digest);
    }

}
